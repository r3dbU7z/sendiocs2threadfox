# SendIOCs2ThreadFox

```diff
-=This script is written by me for learning Python. Try don't laugh at the code, PLS=-
```
![example](https://i.ibb.co/Yym34vj/carbon.png)
![ThreatFix_API](https://i.ibb.co/G5r5BmF/Screenshot-2021-12-21-Threat-Fox-API.png)
![IOCs](https://i.ibb.co/drdbkQF/Screenshot-2021-12-21-Threat-Fox-r3db-U7z.png)

Simple script for share (submit) an IOC to ThreatFox site https://threatfox.abuse.ch

I use this script to send IOCs, these are usually Gafgyt/Mirai botnets.

### WARNING
Use this only after editing and testing. If something explodes there, I'm not going to be responsible for it.

You can see more details about the ThreatFox API here: https://threatfox.abuse.ch/api/#threat-types
